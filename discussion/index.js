// Imports Express package 
const express = require('express')
// express() creates an application which is already a server
const app = express(), port = 3000
// use(...) lets the middleware do common services and capabilities to the application
// Not using these two statements will result in req.body being undefined
app.use(express.json()) // let the app use JSON
app.use(express.urlencoded({ extended: true })) // allows the app to receive data from the forms

// Express has methods corresponding to each HTTP methods
// "/" route expects to recieve GET request at its endpoint 
// res.send(...) sends message as a response to the client

app.get("/", (req, res) => {
    res.send("Hello World")
})

app.get("/hello", (req, res) => {
    res.send("Hello from the /hello endpoint")
})

app.post("/hello", (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

// S29 Activity 
const database = [
    {
        username: "johnny008",
        password: "1111"
    },
    {
        username: "jam009",
        password: "2222"
    },
    {
        username: "beans010",
        password: "3333"
    }
]

app.get("/home", (req, res) => {
    res.send("Welcome to the home page")
})

app.get("/users", (req, res) => {
    res.send(database)
})

app.delete("/delete-user", (req, res) => {
    const username = req.body.username
    const user = database.filter(user => user.username === username)
    if (user.length) {
        let index = 0
        for (let x in database) {
            if (username === database[x].username)
                break
            index++
        }

        database.splice(index, 1) 
        res.send(`User ${username} has been deleted.`)
        console.log(JSON.stringify(database))
    }
    else 
        res.send(`Nobody has been deleted.`)
})

app.post("/signup", (req, res) => {
    const user = {
        username: req.body.username,
        password: req.body.password
    }

    if (user.username && user.password) {
        database.push(user)
        res.send(`User ${user.username} successfully registered.`)
        console.log(JSON.stringify(database))
    }
    else 
        res.send("Please input both username and password")
})

app.listen(port, () => console.log(`Server is running at port:${port}`))